# README #

### What is this repository for? ###

The [not-for-profit organisation] OCEARCH has been busy tagging great white sharks and tracking their movements in near real time, using new technology to advance our understanding of these wondrous sea creatures.
OCEARCH works with 20 different institutions and it even has its own at-sea laboratory.“[Utilising] a custom 75,000 lb.-capacity hydraulic platform designed to safely lift mature sharks for access by a multi-disciplined research team, up to 12 studies are conducted inapproximately 15 minutes on a live mature shark,” the [organisation] says of its M/V OCEARCH vessel. The data collected from these expeditions aids in conservation efforts for great white sharks as well as promoting a better understanding of these apex predators and their role in the ecosystem.
The research – such as tagging and blood sample collection – means only minimal discomfort for the sharks, and there’s been no evidence of long-term problems or pain, according to
OCEARCH (Source: International Business Times).

In this assignment, we developed an application to work with this shark tracking data.

### Functionalities ###

The software connects to a server (using a provided API) in order
to access shark tracking data.

- There is a function to search for sharks that have appeared within a given time frame (in the last 24 hours, in the last week or in the last month).

- It is possible to filter the search for sharks by gender, stage of life and tag location.

- The results of a search displays all pertinent shark details, including species,length, weight and a description of the shark. These results are ordered by recency.

- It is possible to ‘follow’ a shark such that they appear in a list of favourites. Clicking an entry in this list displays more information about that shark.

- Favourite sharks are ordered by the shark’s distance to our current location at King’s (because everyone likes to know how close they are to their favourite sharks).

- It is possible to unfollow a shark, if need be.

- Users are informed if they have a Sharknado anomaly in their favourites list.

- It is possible to arbitrarily switch between the search tool and the favourites list.

- permits the use of different user profiles.

- presents a ‘Shark of the day’ feature.

- shows visual statistics about the types of shark present in a given time period.

- displays a map that depicts the geographical location of all followed sharks


###Team###
- Nikunj Pindoriya
- Serena Chan
- Shivangi Seth
- Arunita Roy