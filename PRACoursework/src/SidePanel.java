import javax.swing.JPanel;
import java.awt.BorderLayout;
import java.awt.GridLayout;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.JSeparator;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.awt.event.ActionEvent;

public class SidePanel extends JPanel {

	private JComboBox<String> cbRange;
	private JComboBox<String> cbGender;
	private JComboBox<String> cbStageofLife;
	private JComboBox<String> cbTagLocation;
	private JButton jbSearch = new JButton("Search");

	
	public SidePanel() {
		setLayout(new BorderLayout(0, 0));

	}

	public void createLayout(JPanel center) {

		JPanel panel = new JPanel();
		add(panel, BorderLayout.CENTER);
		panel.setLayout(new GridLayout(15, 1));

		JLabel lblSharkTracker = new JLabel("Shark Tracker");
		lblSharkTracker.setHorizontalAlignment(SwingConstants.CENTER);
		panel.add(lblSharkTracker);

		panel.add(new JSeparator());
		panel.add(new JLabel("Tracking Range"));

		cbRange = new JComboBox<String>();
		cbRange.setModel(new DefaultComboBoxModel<String>(new String[] { "Last 24 Hours", "Last Week", "Last Month" }));
		panel.add(cbRange);

		panel.add(new JSeparator());

		panel.add(new JLabel("Gender"));

		cbGender = new JComboBox<String>();
		cbGender.setModel(new DefaultComboBoxModel<String>(new String[] { "All", "Male", "Female" }));
		panel.add(cbGender);
		panel.add(new JSeparator());

		panel.add(new JLabel("Stage of Life"));

		cbStageofLife = new JComboBox<String>();
		cbStageofLife.setModel(
				new DefaultComboBoxModel<String>(new String[] { "All", "Mature", "Immature", "Undetermined" }));
		panel.add(cbStageofLife);

		panel.add(new JSeparator());

		panel.add(new JLabel("Tag Location"));

		ArrayList<String> locations = Sorter.getTagLocations();
		locations.add(0, "Any");
		String[] comboLocations = new String[locations.size()];
		comboLocations = locations.toArray(comboLocations);

		cbTagLocation = new JComboBox<String>();
		cbTagLocation.setModel(new DefaultComboBoxModel<String>(comboLocations));//
		panel.add(cbTagLocation);

		jbSearch.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Thread t = new Thread() {

					public void run() {

						getConstraints((CenterPanel) center);
					}
				};
				t.start();
			}
		});

		panel.add(new JSeparator());
		panel.add(jbSearch);

	}

	private void getConstraints(CenterPanel center) {

		HashMap<String, String> constraints = new HashMap<String, String>();
		constraints.put("Tracking Range", (String) cbRange.getSelectedItem());
		constraints.put("Gender", (String) cbGender.getSelectedItem());
		constraints.put("Stage of Life", (String) cbStageofLife.getSelectedItem());
		constraints.put("Tag Location", (String) cbTagLocation.getSelectedItem());
		center.setConstraints(constraints);

	}

}

// minimum panel size
// resize the picture
