import java.util.ArrayList;
import java.util.HashMap;
import java.util.Collections;
import java.util.Comparator;
import api.jaws.*;

public class Sorter {

	private HashMap<String, String> constraints;
	private Jaws jaws;
	private ArrayList<Ping> Pings;
	private static ArrayList<Ping> pastDay;
	private static ArrayList<Ping> pastWeek;
	private static ArrayList<Ping> pastMonth;

	public Sorter() {

		jaws = new Jaws("SVotTaHvx0C0Q3pN", "NELeAgqnwpKwtUfu", true);
	}

	public static void getData() {

		Jaws jaws = new Jaws("SVotTaHvx0C0Q3pN", "NELeAgqnwpKwtUfu", true);
		pastDay = jaws.past24Hours();
		pastWeek = jaws.pastWeek();
		pastMonth = jaws.pastMonth();
		System.out.println("Download Complete!");

	}

	public static ArrayList<String> getTagLocations() {

		Jaws jaws = new Jaws("SVotTaHvx0C0Q3pN", "NELeAgqnwpKwtUfu", true);
		return jaws.getTagLocations();
	}

	public void setConstraints(HashMap<String, String> c) {

		constraints = c;

	}

	public ArrayList<Ping> sortSharks(HashMap<String, String> constraints) {

		setConstraints(constraints);
		return sortSharks();

	}

	public ArrayList<Ping> sortSharks() {

		System.out.println("!sortSharks");
		checkTime();
		System.out.println(Pings.size());
		checkGender();
		System.out.println(Pings.size());
		checkStageofLife();
		System.out.println(Pings.size());
		checkLocation();
		System.out.println(Pings.size());
		finalSort();
		System.out.println(Pings.size());

		return Pings;

	}

	private void checkTime() {

		String time = constraints.get("Tracking Range");

		switch (time) {

		case "Last 24 Hours":
			Pings = pastDay;
			break;

		case "Last Week":
			Pings = pastWeek;
			break;

		case "Last Month":
			Pings = pastMonth;
		}

	}

	private void checkGender() {

		String gender = constraints.get("Gender");
		ArrayList<Ping> sortedGender = new ArrayList<Ping>();

		switch (gender) {

		case "All":
			return;

		case "Male":
			for (Ping p : Pings) {
				if ((jaws.getShark(p.getName()).getGender()).equals("Male")) {
					sortedGender.add(p);
				}
			}
			break;

		case "Female":
			for (Ping p : Pings) {
				if ((jaws.getShark(p.getName()).getGender()).equals("Female")) {
					sortedGender.add(p);
				}
			}
			break;
		}
		Pings = sortedGender;
	}

	private void checkStageofLife() {

		String stage = constraints.get("Stage of Life");
		ArrayList<Ping> sortedStage = new ArrayList<Ping>();

		switch (stage) {

		case "All":
			return;

		case "Mature":
			for (Ping p : Pings) {
				if (((jaws.getShark(p.getName())).getStageOfLife()).equals("Mature")) {
					sortedStage.add(p);
				}
			}
			break;

		case "Immature":
			for (Ping p : Pings) {
				if (((jaws.getShark(p.getName())).getStageOfLife()).equals("Immature")) {
					sortedStage.add(p);
				}
			}
			break;

		case "Undetermined":
			for (Ping p : Pings) {
				if (((jaws.getShark(p.getName())).getStageOfLife()).equals("Undetermined")) {
					sortedStage.add(p);
				}
			}
			break;
		}

		Pings = sortedStage;
	}

	private void checkLocation() {

		String Loc = constraints.get("Tag Location");
		ArrayList<Ping> sortedLocation = new ArrayList<Ping>();

		if (Loc.equals("Any")) {
			return;
		}

		for (Ping p : Pings) {
			if ((jaws.getShark(p.getName()).getTagLocation()).equals(Loc)) {
				sortedLocation.add(p);
			}
		}

		Pings = sortedLocation;
	}

	private void finalSort() {
		// sort time from least recent to most recent
		ArrayList<Ping> nameAndTimeStamps = Pings;
		Collections.sort(nameAndTimeStamps, new Comparator<Ping>() {
			public int compare(Ping o1, Ping o2) {
				return o1.getTime().compareTo(o2.getTime());

			}
		});
		//System.out.println("time sorted from least recent to most" + nameAndTimeStamps);

		// use hashmap to remove duplicates - the time is already sorted
		HashMap<String, Ping> removeSharkDuplicates = new HashMap<String, Ping>();
		for (int i = 0; i < nameAndTimeStamps.size(); i++) {
			removeSharkDuplicates.put(nameAndTimeStamps.get(i).getName(), nameAndTimeStamps.get(i));
			// removes the duplicates of name key. but only retains the most recent
		}
		//System.out.println("removed duplicates" + removeSharkDuplicates);

		// add all the ping values of hashmap into arraylist, then sort the time
		// of each ping from most recent to least, so that we can get the most
		// recent shark from index 0 of the array list
		ArrayList<Ping> values = new ArrayList<Ping>();
		values.addAll(removeSharkDuplicates.values());
		//System.out.println("removeduplicatesarray" + values);
		Collections.sort(values, new Comparator<Ping>() {
			public int compare(Ping o2, Ping o1) {
				return o1.getTime().compareTo(o2.getTime());

			}
		});

		//System.out.println("Most recent sharks array" + values);
		Pings = values;

	}

}
