import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;

import javax.imageio.ImageIO;
import javax.swing.JFrame;
import javax.swing.JPanel;

import api.jaws.Jaws;
import api.jaws.Location;
import api.jaws.Ping;
import api.jaws.Shark;

public class SharkMap extends JPanel {
	private BufferedImage image;
	JFrame jf;
	ArrayList<Ping> sharklocation = new Jaws("SVotTaHvx0C0Q3pN", "NELeAgqnwpKwtUfu").past24Hours();
	public Jaws jaws = new Jaws("SVotTaHvx0C0Q3pN", "NELeAgqnwpKwtUfu");
	String loc="";
	ArrayList<String> fav = new ArrayList<String>();
	ArrayList<Shark> favShark = new ArrayList<Shark>();

	public SharkMap() {
		fav = FavouritesManager.getFavourites();
		System.out.println(fav);
		favv();
		favSharks();
		main();

	}

	public void favv() {
		for (int i = 0; i < fav.size(); i++) {
			favShark.add(jaws.getShark(fav.get(i)));
		}
	}

	public void favSharks() {
		for (Shark s : favShark)
			loc = loc + "markers=color:blue%7Clabel:S%7C" + jaws.getLastLocation(s.getName()).getLatitude() + ","
					+ jaws.getLastLocation(s.getName()).getLongitude() + "&";
		System.out.println(loc);
	}

	public void map() {
		InputStream is;
		try {
			// URL url = new
			// URL("https://maps.googleapis.com/maps/api/staticmap?center=0&zoom=1&size=600x600&markers=color:blue%7Clabel:S%7C40.702147,-74.015794&markers=color:green%7Clabel:G%7C40.711614,-74.012318&markers=color:red%7Clabel:C%7C40.718217,-73.998284&key=AIzaSyCW6pXfo2vq9Xh3vccHDwbv3k9bIxrZZpI");
			URL url = new URL("https://maps.googleapis.com/maps/api/staticmap?center=0&zoom=");
			String zoom = "1";
			URL map1 = new URL (url + zoom + "&size=600x600&" + loc );
			is = map1.openStream();
			image = ImageIO.read(is);
			jf = new JFrame();
			jf.add(this);
			jf.setVisible(true);
			// jf.getContentPane()
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		g.drawImage(image, 0, 0, null);
	}

	public void main() {
		map();
	}
}