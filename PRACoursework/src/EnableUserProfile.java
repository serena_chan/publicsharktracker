import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;


public class EnableUserProfile implements ActionListener{
JTextField textfield;
JButton create;
JButton load;
JFrame jf = new JFrame();
	public EnableUserProfile(){
		setUserProfile();
		jf.pack();
		jf.setVisible(true);
	}
	
	public void setUserProfile(){
		
		JPanel jp = new JPanel(new GridLayout(2, 2));//rows, col
		JLabel enter = new JLabel("Enter name");
		textfield = new JTextField();
	
		JPanel jp2 = new JPanel(new BorderLayout());
		jp.add(enter);
		jp.add(textfield);
		
		load = new JButton("Load");
		load.addActionListener(this);
		
		jp2.add(load);
		
		
		jf.add(jp, BorderLayout.CENTER);
		jf.add(jp2, BorderLayout.SOUTH);
	}
	

	@Override
	public void actionPerformed(ActionEvent e) {
		String n = textfield.getText();
		try {
			//FavouriteManager.loadUserProfile(n);
			FavouriteManager2.setUser(n);
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
	}
	

}
