import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSeparator;

import api.jaws.Jaws;
import api.jaws.Location;

public class Favourites extends JFrame {

	private Jaws jaws = new Jaws("SVotTaHvx0C0Q3pN", "NELeAgqnwpKwtUfu", true);

	public Favourites() {

		super("Favourites");
		setLayout(new BorderLayout());
		setMinimumSize(new Dimension(400, 50));
		add(new JLabel("Your favourite sharks are this far away from you now:"), BorderLayout.NORTH);
		setCenter();
		pack();

	}

	private void setCenter() {

		JPanel center = new JPanel();
		center.setLayout(new BorderLayout());
		JSeparator separator = new JSeparator();
		separator.setPreferredSize(new Dimension (0,20));
		center.add(separator, BorderLayout.NORTH);
		
		
		JPanel favouritesPanel = new JPanel();
		center.add(favouritesPanel, BorderLayout.CENTER);
		favouritesPanel.setLayout(new GridLayout(FavouritesManager.getFavourites().size(), 4));
		
		ArrayList<String> fav = FavouritesManager.getFavourites();
		HashMap<String,Double> favDist = new HashMap<String, Double>();
		
		for (String f : fav) {
			
			favDist.put(f, Math.floor((haversine(jaws.getLastLocation(f))) * 100) / 100);
		}
		
		
		Collections.sort(fav, new Comparator<String>() {
			
	        @Override public int compare(String p1, String p2) {
	            return Double.compare(favDist.get(p1), favDist.get(p2)); // Ascending
	        }

	    });

		for (String s : fav) {

			favouritesPanel.add(new JLabel(s));
			String distance = favDist.get(s) + " km";
			favouritesPanel.add(new JLabel(distance));
			JButton view = new JButton("View details");
			view.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent arg0) {
					SearchFrame sf = new SearchFrame(s);
					sf.setVisible(true);
				}
			});
			favouritesPanel.add(view);
			
			JButton unfollow = new JButton("Unfollow");
			unfollow.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent arg0) {
					
					FavouritesManager.removeFavourite(s);
					remove(center);
					setCenter();
					MenuFrame.favouritesCheck();
					revalidate();
				}
			});
			favouritesPanel.add(unfollow);


		}

		add(center, BorderLayout.CENTER);
	}

	private double haversine(Location loc) {

		double R = 6372.8; // In kilometers change for different units
		Location kings = new Location(51.511851,-0.116138);

		double lat2 = kings.getLatitude();
		double lon2 = kings.getLongitude();

		double lat1 = loc.getLatitude();
		double lon1 = loc.getLongitude();

		double dLat = Math.toRadians(lat2 - lat1);
		double dLon = Math.toRadians(lon2 - lon1);
		lat1 = Math.toRadians(lat1);
		lat2 = Math.toRadians(lat2);

		double a = Math.pow(Math.sin(dLat / 2), 2) + Math.pow(Math.sin(dLon / 2), 2) * Math.cos(lat1) * Math.cos(lat2);
		double c = 2 * Math.asin(Math.sqrt(a));
		return R * c;
	}

}

