import java.awt.GridLayout;
import java.util.ArrayList;
import java.util.HashMap;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import api.jaws.*;

public class CenterPanel extends JPanel {

	private Jaws jaws;

	public CenterPanel() {

		// jaws = new Jaws("SVotTaHvx0C0Q3pN", "NELeAgqnwpKwtUfu", true);

	}

	public void setConstraints(HashMap<String, String> constraints) {

		Sorter sorter = new Sorter();
		removeAll();
		ArrayList<Ping> Pings = sorter.sortSharks(constraints);

		setLayout(new GridLayout(Pings.size(), 1));

		if (Pings.isEmpty()) {
			setLayout(new GridLayout(1, 1));
			JLabel nosharks = new JLabel("No Sharks Found!");
			add(nosharks);
			nosharks.setHorizontalAlignment(SwingConstants.CENTER);

		} else {

			for (Ping p : Pings) {

				add(new SharkPanel(p));

			}
		}

		revalidate();

	}

	public void setConstraints(String sharkName) {

		Sorter sorter = new Sorter();
		removeAll();

		HashMap<String, String> constraints = new HashMap<String, String>();
		constraints.put("Tracking Range", "Last Month");
		constraints.put("Gender", "All");
		constraints.put("Stage of Life", "All");
		constraints.put("Tag Location", "Any");

		ArrayList<Ping> Pings = sorter.sortSharks(constraints);
		Ping sharkPing = null;

		for (Ping p : Pings) {

			if (p.getName().equals(sharkName)) {
				sharkPing = p;
			}
		}

		setLayout(new GridLayout(1, 1));

		if (Pings.isEmpty()) {
			setLayout(new GridLayout(1, 1));
			JLabel nosharks = new JLabel("No Sharks Found!");
			add(nosharks);
			nosharks.setHorizontalAlignment(SwingConstants.CENTER);

		} else {

			add(new SharkPanel(sharkPing));

		}
		revalidate();

	}
}
