import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class FavouritesManager {

	private static ArrayList<String> favouritesArray = new ArrayList<String>();
	private static String User = "default";
	private static File directory = new File(User);

	public static void setUser() throws FileNotFoundException {
		// use ONLY for default user, use at the start of the program

		User = "default";
		directory = new File(User);
		try {
			if (!(directory.exists())) {
				directory.createNewFile();
			}
		} catch (IOException e1) {

			e1.printStackTrace();
		}
		loadProfile();
		System.out.println(User);

	}

	public static void setUser(String n) throws FileNotFoundException {
		// use to create a user or for existing users

		User = n;
		directory = new File(n);
		try {
			if (!(directory.exists())) {
				directory.createNewFile();
			}
		} catch (IOException e1) {

			e1.printStackTrace();
		}
		loadProfile();
		System.out.println(User);

	}

	private static void loadProfile() {

		FileInputStream fis;
		favouritesArray.clear();
		try {
			fis = new FileInputStream(directory);

			BufferedReader br = new BufferedReader(new InputStreamReader(fis));

			String line = null;
			while ((line = br.readLine()) != null) {
				favouritesArray.add(line);
			}

			br.close();

		} catch (IOException e) {

			e.printStackTrace();
		}
	}

	public static ArrayList<String> getFavourites() {
		// use to get the list of favourite sharks
		return favouritesArray;
	}

	public static void addFavourite(String sharkName) {

		if (!(favouritesArray.contains(sharkName))) {

			favouritesArray.add(sharkName);

			try {

				PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(directory, true)));

				out.println(sharkName);
				out.close();

			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public static void removeFavourite(String sharkName) {
		// Use to remove a favourite shark

		if (favouritesArray.contains(sharkName)) {

			favouritesArray.remove(sharkName);

			ArrayList<String> temp = new ArrayList<String>();
			for (String s : favouritesArray) {
				temp.add(s);
			}

			// Create new file for updated Favourites and remove the old
			directory.delete();
			try {
				setUser(User);
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}

			// Restore the Favourites
			for (String s : temp) {
				addFavourite(s);
			}

		}
	}

	public static void deleteUser(String s) {

		try {
			setUser(User);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

		directory.delete();
	}
}