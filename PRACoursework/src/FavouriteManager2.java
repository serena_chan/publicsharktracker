

	import java.io.BufferedReader;
	import java.io.BufferedWriter;
	import java.io.File;
	import java.io.FileInputStream;
	import java.io.FileNotFoundException;
	import java.io.FileWriter;
	import java.io.IOException;
	import java.io.InputStreamReader;
	import java.io.PrintWriter;
	import java.util.ArrayList;

	public class FavouriteManager2 {

		private static ArrayList<String> favouritesArray = new ArrayList<String>();
		private static String User = "default";
		private static File directory = new File(User);

		public static void setUser() throws FileNotFoundException {
			// use ONLY for default user, use at the start of the program
			
			User = "default"; //why do i have to put user = default here when it was already done in the field? so you can pass user into directory 
			directory = new File(User);
			try {
				if (!(directory.exists())) {
					directory.createNewFile();
				}
			} catch (IOException e1) {

				e1.printStackTrace();
			}
			loadProfile();
			System.out.println(User);

		}

		
		public static void setUser(String n) throws FileNotFoundException {
			// use to create a user or for existing users
			
			User = n;
			directory = new File(n);
			try {
				if (!(directory.exists())) {
					directory.createNewFile();
				}
			} catch (IOException e1) {

				e1.printStackTrace();
			}
			loadProfile();
			System.out.println(User);

		}

		/**
		 * User trying to load profile that doesn't exist --> 
		 */
		private static void loadProfile() {

			FileInputStream fis;
			favouritesArray.clear();	
			try {
				fis = new FileInputStream(directory);//fileinputstream obtains data from the file with name of directory variable

				BufferedReader br = new BufferedReader(new InputStreamReader(fis));//buffered reader is like a efficient reader

				String line = null;
				while ((line = br.readLine()) != null) { //if the line is not null then add the line to favouritesarray
					favouritesArray.add(line);
				}

				br.close();

			} catch (IOException e) {

				e.printStackTrace();
			}
		}

		
		public static ArrayList<String> getFavourites() {
			// use to get the list of favourite sharks
			return favouritesArray;
		}

		public static void addFavourite(String sharkName) {

			if (!(favouritesArray.contains(sharkName))) {//if the favourites array doesn't contain the sharkname

				favouritesArray.add(sharkName);

				try {

					PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(directory, true)));	//make a printwriter that has a file and the words will be added to the file

					out.println(sharkName);
					out.close();

				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}


		public static void removeFavourite(String sharkName) {
			// Use to remove a favourite shark
			
			if (favouritesArray.contains(sharkName)) {

				favouritesArray.remove(sharkName);

				ArrayList<String> temp = new ArrayList<String>();
				for (String s : favouritesArray) {
					temp.add(s);
				}

				// Create new file for updated Favourites and remove the old
				directory.delete();
				try {
					setUser(User);
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				}

				// Restore the Favourites
				for (String s : temp) {
					addFavourite(s);
				}

			}
		}

	}

