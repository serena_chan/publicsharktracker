
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.util.ArrayList;

//a static class so you can get the user profile anywhere in the program
public class FavouriteManager {

private static ArrayList<String> favouriteArray = new ArrayList<String>();
private static String User = "default";

	public static void setUser(String n) throws FileNotFoundException{
		User = n;
		File directory = new File(n);
		try {
			boolean a = directory.createNewFile();//true if the named file does not exist and was successfully created; false if the named file already exists
			System.out.println(a);
			
			if (a == true) {//true if the named file does not exist and was successfully created. user tries to load a profile that does not exist --> create new file
				System.out.println("Creating new records of favourite sharks for "+n);//settext jlabel
			}
			
			if(a == false){//false if the named file already exists. user tries to create a user profile with the same name as a profile that already exists --> load that profile
				System.out.println("Load Existing User Profile of " +n);
				System.out.println("Enter another name");
				//now either enter another name or go to loadexistinguserprofile
			}
			
			//if user creates profile with same name as a profile that already exists, then a= false, print out no.
			//user tries to load profile that doesn't exist : a = true, user creates a file 
		} catch (IOException e) {
			e.printStackTrace();
		}
           
	}
	
	public static void createUserProfile(String n) throws FileNotFoundException{
		User = n;
		File directory = new File(n);
		
			try {
				if (directory.exists() == false) {
					System.out.println("Creating new records of favourite sharks for "+n);//settext jlabel
					boolean test = directory.createNewFile();
					System.out.println(test);
				} else if (directory.exists() == true){//if user creates profile with same name as a profile that already exists
					System.out.println("Name " +n + " already exists, enter another name or load existing");
					//after showmessagedialog now either enter another name or go to loadexistinguserprofile 
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
				
		
	}
	
	public static void loadUserProfile(String n) throws FileNotFoundException{
		User = n;
		File directory = new File(n);
	
		try {
			if (directory.exists() == true) {
					boolean test = directory.createNewFile();
					System.out.println(test);//this should print out false because file already exists
			}else if(directory.exists() == false){//if user tries to load profile that doesn't exist yet
				System.out.println("Name " +n + " doesn't exist, please create new profile instead");
			}
		}catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	
	}
            
	public static void loadFavourites(String sharkName){
		//reads favourites shark names and saves in a static arraylist of sharknames
		favouriteArray.add(sharkName);
		
		
	}
	

	public static ArrayList<String> getFavourites(){ //return arraylist favourites
		return favouriteArray;
	}
	

	
	public static void writeToFile(String sharkName){
		
		try (Writer writer = new BufferedWriter(new OutputStreamWriter(
	              new FileOutputStream(User, true), "utf-8"))) {	
	   writer.write(sharkName);
	   ((BufferedWriter) writer).newLine();
	   
	} catch (UnsupportedEncodingException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} catch (FileNotFoundException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	}
	

	
	
}
