import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

public class MenuFrame extends JFrame {

	private JFrame frame = new JFrame("Amenity Police");
	protected static JButton button2 = new JButton("Favourites");

	public MenuFrame() {

		setlayout();
	}

	private void setlayout() {

		JButton button1 = new JButton("Search");
		button1.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {

				SearchFrame sf = new SearchFrame();
				sf.setVisible(true);

			}
		});

		favouritesCheck();
		button2.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {

				Favourites f = new Favourites();
				f.setVisible(true);

			}

		});
		JButton labelsharkoftheday = new JButton("Shark of the day");
		labelsharkoftheday.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {

				SharkoftheDay sd = new SharkoftheDay();
				sd.ShowFrame();

			}

		});
		JButton statistics = new JButton("Statistics");

		statistics.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {

				Statistics stats = new Statistics();
				stats.pack();
				stats.setVisible(true);
			}
		});

		JPanel bottomone = new JPanel();
		JPanel bottomtwo = new JPanel();
		JPanel buttonarea = new JPanel();

		ImageIcon image = new ImageIcon(MenuFrame.class.getResource("0x600.jpg"));
		JLabel label = new JLabel(image);

		setLayout(new BorderLayout());
		buttonarea.setLayout(new BorderLayout());
		bottomone.setLayout(new BorderLayout());
		bottomtwo.setLayout(new BorderLayout());

		frame.add(label, BorderLayout.CENTER);

		bottomone.add(button1, BorderLayout.NORTH);
		bottomone.add(button2, BorderLayout.CENTER);

		bottomtwo.add(labelsharkoftheday, BorderLayout.NORTH);
		bottomtwo.add(statistics, BorderLayout.CENTER);

		buttonarea.add(bottomone, BorderLayout.NORTH);
		buttonarea.add(bottomtwo, BorderLayout.SOUTH);

		frame.add(buttonarea, BorderLayout.SOUTH);
		frame.setSize(250, 250);
		setMenuBar();
		frame.setVisible(true);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.pack();
	}

	protected static void favouritesCheck() {

		if (FavouritesManager.getFavourites().size() == 0) {

			button2.setEnabled(false);
		} else {
			button2.setEnabled(true);
		}
	}

	private void setMenuBar() {

		JMenuBar menuBar = new JMenuBar();
		frame.setJMenuBar(menuBar);

		JMenu fileMenu = new JMenu("File");
		menuBar.add(fileMenu);
		JMenuItem userAction = new JMenuItem("Set User");
		fileMenu.add(userAction);
		userAction.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {

				String user_name = null;
				user_name = JOptionPane.showInputDialog("Enter your name:");

				if (user_name == null) {

					return;
				}

				if (user_name.equals("")) {
					try {
						FavouritesManager.setUser();
						favouritesCheck();

					} catch (FileNotFoundException e) {
						e.printStackTrace();
					}
					user_name = "default";
				} else {
					try {
						FavouritesManager.setUser(user_name);
						favouritesCheck();

					} catch (FileNotFoundException e) {
						e.printStackTrace();
					}
				}

				JOptionPane.showMessageDialog(null, "Welcome " + user_name);

			}

		});

	}

}