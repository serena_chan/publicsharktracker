import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.SwingConstants;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PiePlot;
import org.jfree.data.general.DefaultPieDataset;
import api.jaws.Jaws;
import api.jaws.Ping;

public class Statistics extends JFrame {

	private String time;
	private HashMap<String, String> constraints = new HashMap<String, String>();
	private JPanel center;
	private Sorter sorter;
	private Jaws jaws = new Jaws("SVotTaHvx0C0Q3pN", "NELeAgqnwpKwtUfu", true);
	private ArrayList<Ping> pings;
	private JScrollPane scrollPane;

	public Statistics() {

		super("Statistics");
		setPreferredSize(new Dimension(900, 700));
		sorter = new Sorter();
		setLayout(new BorderLayout());
		center = new JPanel();
		scrollPane = new JScrollPane(center);
		add(scrollPane, BorderLayout.CENTER);
	
		createView();
	}

	private void createView() {

		JPanel side = new JPanel();
		side.setLayout(new BorderLayout());
		add(side, BorderLayout.WEST);

		JPanel sideInner = new JPanel();
		side.add(sideInner, BorderLayout.NORTH); // Fix
		sideInner.setLayout(new GridLayout(6, 1));

		JLabel lblStats = new JLabel("Statistics Viewer");
		lblStats.setHorizontalAlignment(SwingConstants.CENTER);
		sideInner.add(lblStats);

		sideInner.add(new JSeparator());
		sideInner.add(new JLabel("Tracking Range                                "));

		JComboBox<String> cbRange = new JComboBox<String>();
		cbRange.setModel(new DefaultComboBoxModel<String>(new String[] { "Last 24 Hours", "Last Week", "Last Month" }));
		sideInner.add(cbRange);

		sideInner.add(new JSeparator());
		JButton jbGo = new JButton("Go");
		sideInner.add(jbGo);

		jbGo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				time = (String) cbRange.getSelectedItem();
				setGeneralConstraints();
				pings = sorter.sortSharks(constraints);
				remove(scrollPane);
				createPie();

			}
		});
	}

	private void setGeneralConstraints() {

		constraints.put("Tracking Range", time);
		constraints.put("Gender", "All");
		constraints.put("Stage of Life", "All");
		constraints.put("Tag Location", "Any");

	}

	private void createPie() {

		center = new JPanel();
		scrollPane = new JScrollPane(center);
		add(scrollPane, BorderLayout.CENTER);
		center.setLayout(new GridLayout(3, 1));
		center.add(genderPie());
		center.add(stageofLifePie());
		center.add(locationPie());
		revalidate();

	}

	private JPanel genderPie() {

		HashMap<String, Integer> count = new HashMap<String, Integer>();

		for (Ping p : pings) {

			String gender = (jaws.getShark(p.getName())).getGender();
			if (count.containsKey(gender)) {

				count.put(gender, count.get(gender) + 1);
			} else {
				count.put(gender, 1);
			}
		}

		DefaultPieDataset genderDataset = new DefaultPieDataset();

		genderDataset.setValue("Male", count.get("Male"));
		genderDataset.setValue("Female", count.get("Female"));

		JFreeChart chart = ChartFactory.createPieChart("Gender", genderDataset, true, true, false);
		PiePlot plot = (PiePlot) chart.getPlot();
		plot.setLabelFont(new Font("SansSerif", Font.PLAIN, 12));
		plot.setNoDataMessage("No data available");
		plot.setCircular(false);
		plot.setLabelGap(0.02);

		return new ChartPanel(chart);

	}

	private JPanel stageofLifePie() {

		HashMap<String, Integer> count = new HashMap<String, Integer>();

		for (Ping p : pings) {

			String sof = (jaws.getShark(p.getName())).getStageOfLife();
			if (count.containsKey(sof)) {

				count.put(sof, count.get(sof) + 1);
			} else {
				count.put(sof, 1);
			}
		}

		DefaultPieDataset stageofLifeDataset = new DefaultPieDataset();

		stageofLifeDataset.setValue("Mature", count.get("Mature"));
		stageofLifeDataset.setValue("Immature", count.get("Immature"));
		stageofLifeDataset.setValue("Undetermined", count.get("Undetermined"));

		JFreeChart chart = ChartFactory.createPieChart("Stage of Life", stageofLifeDataset, true, true, false);
		PiePlot plot = (PiePlot) chart.getPlot();
		plot.setLabelFont(new Font("SansSerif", Font.PLAIN, 12));
		plot.setNoDataMessage("No data available");
		plot.setCircular(false);
		plot.setLabelGap(0.02);

		return new ChartPanel(chart);

	}

	private JPanel locationPie() {

		HashMap<String, Integer> count = new HashMap<String, Integer>();
		DefaultPieDataset locationDataset = new DefaultPieDataset();

		for (Ping p : pings) {

			String loc = (jaws.getShark(p.getName())).getTagLocation();
			if (count.containsKey(loc)) {

				count.put(loc, count.get(loc) + 1);
			} else {
				count.put(loc, 1);
			}
		}

		for (String s : Sorter.getTagLocations()) {

			if (!(count.get(s) == null)) {

				locationDataset.setValue(s, count.get(s));
			}
		}

		JFreeChart chart = ChartFactory.createPieChart("Tag Locations", locationDataset, true, true, false);
		PiePlot plot = (PiePlot) chart.getPlot();
		plot.setLabelFont(new Font("SansSerif", Font.PLAIN, 12));
		plot.setNoDataMessage("No data available");
		plot.setCircular(false);
		plot.setLabelGap(0.02);
		return new ChartPanel(chart);

	}
}
