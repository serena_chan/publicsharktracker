import java.awt.BorderLayout;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import javax.swing.JFrame;
import javax.swing.JLabel;

import api.jaws.Jaws;
import api.jaws.Ping;

public class SharkoftheDay {
	// cache?
	private ArrayList<Ping> sharkoftheday = new Jaws("SVotTaHvx0C0Q3pN", "NELeAgqnwpKwtUfu").past24Hours();

	public void setSharkoftheDay(String sharkName, String time) {

		File SharkoftheDay = new File("SharkoftheDay");

		if (SharkoftheDay.exists()) {
			SharkoftheDay.delete();
		}

		try {

			SharkoftheDay.createNewFile();
		} catch (IOException e1) {

			e1.printStackTrace();
		}
		try {

			PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(SharkoftheDay, true)));

			out.println(sharkName);
			out.println(time);
			out.close();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public boolean isSharkValid() {
		ArrayList<String> sharkInfo = new ArrayList<String>();
		File SharkoftheDay = new File("SharkoftheDay");
		if (!(SharkoftheDay.exists())) {
			return false;
		}
		try {
			FileInputStream fis = new FileInputStream(SharkoftheDay);

			BufferedReader br = new BufferedReader(new InputStreamReader(fis));

			String line = null;
			while ((line = br.readLine()) != null) {
				sharkInfo.add(line);
			}

			br.close();

		} catch (IOException e) {

			e.printStackTrace();
		}
		DateFormat dateFormat1 = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		DateFormat dateFormat2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date date1 = null;
		/*
		 * if (true) { System.out.println(sharkInfo.get(1)); return false; }
		 */
		try {
			date1 = dateFormat1.parse(sharkInfo.get(1));// sharks time
		} catch (ParseException e) {
			try {
				date1 = dateFormat2.parse(sharkInfo.get(1));// sharks time
			} catch (ParseException f) {
				f.printStackTrace();
			}
		}
		Date date2 = new Date();// current time
		long difference = date2.getTime() - date1.getTime();
		if (difference <= 86400000) {
			return true;
		} else {
			return false;
		}
	}

	public String getSharkoftheDay() {
		try {
			FileInputStream fis = new FileInputStream(new File("SharkoftheDay"));

			BufferedReader br = new BufferedReader(new InputStreamReader(fis));

			String line = br.readLine();
			br.close();
			return line;

		} catch (IOException e) {

			e.printStackTrace();
		}
		return null;
	}

	/**
	 * ShowFrame shows the frame with shark of the day url obtained from
	 * jaws.getVideo(). 1. put URL of a random shark to JFrame 2. after 24 hours
	 * change random shark thus shark URL by calling isSharkValid(). If
	 * isSharkValid == false, that means 24 hours has changed. Then you will set
	 * a new shark using setSharkName()
	 */

	public void ShowFrame() {
		boolean valid = isSharkValid();
		if (valid == false) {
			setSharkoftheDay(sharkoftheday.get(0).getName(), sharkoftheday.get(0).getTime());
		} else {
			JFrame video = new JFrame("Shark of the Day");
			video.setSize(500, 500);
			JLabel nameofsharkoftheday = new JLabel();
			nameofsharkoftheday.setText(getSharkoftheDay());
			video.setLayout(new BorderLayout());
			video.add(nameofsharkoftheday, BorderLayout.NORTH);
			Jaws jaws = new Jaws("SVotTaHvx0C0Q3pN", "NELeAgqnwpKwtUfu");
			System.out.println(jaws.getVideo(getSharkoftheDay()));
			video.pack();
			video.setVisible(true);

		}

	}
}
