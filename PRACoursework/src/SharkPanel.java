import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.JTextArea;
import api.jaws.Jaws;
import api.jaws.Ping;
import api.jaws.Shark;

/*right hand side of search frame*/
public class SharkPanel extends JPanel {

	private Shark shark;
	private Jaws jaws = new Jaws("SVotTaHvx0C0Q3pN", "NELeAgqnwpKwtUfu");
	private String time;
	private JPanel main = new JPanel();;

	public SharkPanel(Ping p) {

		setLayout(new BorderLayout());
		shark = jaws.getShark(p.getName());
		main.setLayout(new BorderLayout(3, 10)); // int hgap,int vgap
		time = p.getTime();
		north(shark);
		center(shark);
		south(time);
		add(main, BorderLayout.CENTER);
		add(new JSeparator(), BorderLayout.SOUTH);

	}

	private void north(Shark shark) {
		JLabel name = new JLabel("Name: ");

		JLabel namedes = new JLabel(shark.getName());
		JLabel gender = new JLabel("Gender: ");
		JLabel genderdes = new JLabel(shark.getGender());
		JLabel stageoflife = new JLabel("Stage of Life: ");
		JLabel stageoflifedes = new JLabel(shark.getStageOfLife());
		JLabel species = new JLabel("Species: ");
		JLabel speciesdes = new JLabel(shark.getSpecies());
		JLabel length = new JLabel("Length: ");
		JLabel lengthdes = new JLabel(shark.getLength());
		JLabel weight = new JLabel("Weight: ");
		JLabel weightdes = new JLabel(shark.getWeight());

		JPanel northpanel = new JPanel(new GridLayout(6, 2, 1, 1)); // 6 rows, 2
																	// cols,1
																	// hgap,
																	// 0vgap
		northpanel.add(name);
		northpanel.add(namedes);
		northpanel.add(gender);
		northpanel.add(genderdes);
		northpanel.add(stageoflife);
		northpanel.add(stageoflifedes);
		northpanel.add(species);
		northpanel.add(speciesdes);
		northpanel.add(length);
		northpanel.add(lengthdes);
		northpanel.add(weight);
		northpanel.add(weightdes);

		main.add(northpanel, BorderLayout.NORTH);

	}

	private void center(Shark shark) {
		JLabel description = new JLabel("Description: ");
		JTextArea descriptionbox = new JTextArea(shark.getDescription());

		descriptionbox.setLineWrap(true);
		descriptionbox.setOpaque(false);
		descriptionbox.setEditable(false);

		JPanel descriptionPanel = new JPanel(new GridLayout(2, 1, 0, 3));
		descriptionPanel.add(description);
		descriptionPanel.add(descriptionbox);
		main.add(descriptionPanel, BorderLayout.CENTER);

	}

	private void south(String time) {
		JLabel lastPing = new JLabel("Last Ping: " + time);
		JButton follow = new JButton("Follow");
		checkFollowStatus(follow);
		follow.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {

				if (follow.getText().equals("Following")) {

					FavouritesManager.removeFavourite(shark.getName());
					checkFollowStatus(follow);

				} else if (follow.getText().equals("Follow")) {

					FavouritesManager.addFavourite(shark.getName());
					checkFollowStatus(follow);
				}

				MenuFrame.favouritesCheck();
			}
		});
		JPanel southpanel = new JPanel(new GridLayout(1, 2, 0, 3));
		// 1 rows, 2 cols, 0hgap, 3vgap
		southpanel.add(lastPing);
		southpanel.add(follow);
		lastPing.setAlignmentX(Component.LEFT_ALIGNMENT);
		follow.setAlignmentX(Component.RIGHT_ALIGNMENT);
		main.add(southpanel, BorderLayout.SOUTH);
	}

	private void checkFollowStatus(JButton follow) {

		if (FavouritesManager.getFavourites().contains(shark.getName())) {

			follow.setText("Following");
		} else {
			follow.setText("Follow");
		}

	}

}
