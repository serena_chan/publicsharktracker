import java.awt.BorderLayout;
import api.jaws.Jaws;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;

public class SearchFrame extends JFrame {
	
	private CenterPanel center = new CenterPanel();
	private SidePanel sidePanel = new SidePanel();
	private JScrollPane scrollPane = new JScrollPane(center);
	
	public SearchFrame() {
		
		super("Search");
		setup();
		pack();

	}
	
	public SearchFrame(String name) {
		
		super("Search");
		setup();
		center.setConstraints(name);
		pack();
	}
	
	private void setup() {
		
		Jaws jaws = new Jaws("SVotTaHvx0C0Q3pN","NELeAgqnwpKwtUfu",true);
		setLayout(new BorderLayout());
		add(new JLabel(jaws.getAcknowledgement()),BorderLayout.SOUTH);
		add(sidePanel, BorderLayout.WEST);
		add(scrollPane, BorderLayout.CENTER);
		sidePanel.createLayout(center);
		
	}
}
